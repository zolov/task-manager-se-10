package ru.zolov.tm.api;

import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.UserExistException;
import ru.zolov.tm.exception.UserNotFoundException;

public interface IUserService {

  @Nullable User getCurrentUser();

  void setCurrentUser(@Nullable User currentUser);

  boolean isAuth();

  @Nullable User login(@Nullable String login, @Nullable String password)
  throws EmptyStringException, UserNotFoundException;

  void userRegistration(@Nullable String login, @Nullable String password)
  throws EmptyStringException, UserExistException;

  void adminRegistration(@Nullable String login, @Nullable String password)
  throws EmptyStringException;

  void logOut();

  boolean isRolesAllowed(@Nullable RoleType... roleTypes);

  @Nullable User findByLogin(@Nullable String login) throws EmptyStringException, UserNotFoundException;

  @Nullable User findById(@Nullable String id) throws EmptyStringException, UserNotFoundException;

  @NotNull List<User> readAll() throws EmptyRepositoryException;

  boolean remove(@Nullable String id) throws EmptyStringException;

  void updateUserPassword(@Nullable String id, @Nullable String password)
  throws EmptyStringException, UserNotFoundException;

  void load(@Nullable final List<User> list) throws EmptyRepositoryException;
}
