package ru.zolov.tm.api;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.AbstractGoal;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public interface ITerminalService {

  String nextLine();

  Integer nextInt();

  @NotNull Comparator<AbstractGoal> getComparator(@Nullable String name) throws EmptyStringException;

  void performGoal(@Nullable AbstractGoal goal, @Nullable String userName)
  throws EmptyRepositoryException, EmptyStringException;

  void performList(@Nullable List<? extends AbstractGoal> list, @Nullable String userName)
  throws EmptyRepositoryException, EmptyStringException;

  String performDate(@Nullable Date date);
}
