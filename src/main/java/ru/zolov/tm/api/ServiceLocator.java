package ru.zolov.tm.api;

import java.util.List;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.command.AbstractCommand;

public interface ServiceLocator {

  @NotNull IProjectService getProjectService();

  @NotNull ITaskService getTaskService();

  @NotNull ITerminalService getTerminalService();

  @NotNull List<AbstractCommand> getCommandList();

  @NotNull IUserService getUserService();

  @NotNull IDomainService getDomainService();
}
