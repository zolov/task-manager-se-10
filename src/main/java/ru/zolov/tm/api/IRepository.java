package ru.zolov.tm.api;

import java.util.List;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.entity.AbstractEntity;

public interface IRepository<E extends AbstractEntity> {

  void load(@NotNull final List<E> list);
}
