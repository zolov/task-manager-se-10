package ru.zolov.tm.api;

import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.exception.EmptyRepositoryException;

public interface IUserRepository extends IRepository<User> {

  @NotNull User persist(@NotNull User user);

  @NotNull List<User> findAll() throws EmptyRepositoryException;

  @Nullable User findOne(@NotNull String id);

  @Nullable User findByLogin(@NotNull String login);

  void merge(@NotNull User user);

  boolean remove(@NotNull String id);

  void removeAll();

  void update(@NotNull String id, @NotNull String login);
}
