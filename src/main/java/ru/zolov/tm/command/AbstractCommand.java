package ru.zolov.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.api.ServiceLocator;
import ru.zolov.tm.enumerated.RoleType;

public abstract class AbstractCommand implements Comparable<AbstractCommand> {

  protected ServiceLocator serviceLocator;

  public void setServiceLocator(ServiceLocator serviceLocator) {
    this.serviceLocator = serviceLocator;
  }

  public abstract String getName();

  public abstract String getDescription();

  public abstract boolean secure();

  public abstract void execute()
      throws
      Exception;

  public abstract RoleType[] roles();

  @Override
  public int compareTo(@NotNull AbstractCommand abstractCommand) {
    return this.getName().compareTo(abstractCommand.getName());
  }
}
