package ru.zolov.tm.command.task;

import java.text.ParseException;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public final class TaskEditCommand extends AbstractCommand {

  private final String name = "task-edit";
  private final String description = "Edit task";

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public boolean secure() {
    return false;
  }

  @Override
  public void execute() throws EmptyStringException, EmptyRepositoryException, ParseException {
    @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getId();
    @NotNull final String userName = serviceLocator.getUserService().getCurrentUser().getLogin();
    @NotNull final List<Task> listOfTasks = serviceLocator.getTaskService().readAll(userId);
    serviceLocator.getTerminalService().performList(listOfTasks, userName);
    System.out.print("Enter task id: ");
    final String id = serviceLocator.getTerminalService().nextLine();
    System.out.print("Enter new task name: ");
    final String name = serviceLocator.getTerminalService().nextLine();
    System.out.print("Enter new description: ");
    final String description = serviceLocator.getTerminalService().nextLine();
    System.out.println("Enter date of project start in format DD.MM.YYYY: ");
    final String start = serviceLocator.getTerminalService().nextLine();
    System.out.println("Enter date of project finish in format DD.MM.YYYY: ");
    final String finish = serviceLocator.getTerminalService().nextLine();
    serviceLocator.getTaskService().update(userId, id, name, description, start, finish);
  }

  @Override
  public RoleType[] roles() {
    return new RoleType[]{RoleType.ADMIN, RoleType.USER};
  }
}
