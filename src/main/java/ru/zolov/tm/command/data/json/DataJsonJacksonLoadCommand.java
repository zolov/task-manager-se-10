package ru.zolov.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.entity.Domain;
import ru.zolov.tm.enumerated.PathEnum;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public class DataJsonJacksonLoadCommand extends AbstractCommand {

  private final String name = "dataload-json-jackson";
  private final String description = "Load data from json file (jackson)";

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public boolean secure() {
    return true;
  }

  @Override
  public void execute()
      throws
      IOException,
      EmptyStringException,
      EmptyRepositoryException {
    System.out.println(description);
    @NotNull
    final File file = new File(PathEnum.JSON.getPath());
    @NotNull
    final ObjectMapper objectMapper = new ObjectMapper();
    @Nullable
    final Domain domain = objectMapper.readValue(file, Domain.class);
    serviceLocator.getDomainService().load(domain);
    System.out.println("Load data from " + file.getPath());
  }

  @Override
  public RoleType[] roles() {
    return new RoleType[]{RoleType.ADMIN, RoleType.USER};
  }
}
