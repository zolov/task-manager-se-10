package ru.zolov.tm.command.data.xml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import java.io.File;
import java.io.IOException;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.entity.Domain;
import ru.zolov.tm.enumerated.PathEnum;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public class DataXmlJacksonLoadCommand extends AbstractCommand {

  private final String name = "dataload-xml-jackson";
  private final String description = "Load data from xml file (jackson)";

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public boolean secure() {
    return true;
  }

  @Override
  public void execute()
      throws
      IOException,
      EmptyStringException,
      EmptyRepositoryException {
    System.out.println(description);
    @NotNull
    final File file = new File(PathEnum.XML.getPath());
    @NotNull
    final ObjectMapper objectMapper = new XmlMapper();
    @NotNull
    final Domain domain = objectMapper.readValue(file, Domain.class);
    serviceLocator.getDomainService().load(domain);
    System.out.println("Data load from " + file.getPath());
  }

  @Override
  public RoleType[] roles() {
    return new RoleType[]{RoleType.ADMIN, RoleType.USER};
  }
}
