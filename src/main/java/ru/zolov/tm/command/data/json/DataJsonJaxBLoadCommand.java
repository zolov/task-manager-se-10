package ru.zolov.tm.command.data.json;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.entity.Domain;
import ru.zolov.tm.enumerated.PathEnum;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public class DataJsonJaxBLoadCommand extends AbstractCommand {

  private final String name = "dataload-json-jaxb";
  private final String description = "Load data from json file (jaxb)";

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public boolean secure() {
    return true;
  }

  @Override
  public void execute()
      throws
      JAXBException,
      EmptyStringException,
      EmptyRepositoryException {
    System.out.println(description);
    final File file = new File(PathEnum.JAXBJSON.getPath());
    Map<String, Object> properties = new HashMap<String, Object>(2);
    properties.put(JAXBContextProperties.MEDIA_TYPE, "application/json");
    properties.put(JAXBContextProperties.JSON_INCLUDE_ROOT, Boolean.TRUE);
    @NotNull
    final JAXBContext jaxbContext = JAXBContext.newInstance(new Class[]{Domain.class}, properties);
    @NotNull
    final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
    @NotNull
    final Domain domain = (Domain)unmarshaller.unmarshal(file);
    serviceLocator.getDomainService().load(domain);
    System.out.println("Load data from " + file.getPath());
  }

  @Override
  public RoleType[] roles() {
    return new RoleType[]{RoleType.ADMIN, RoleType.USER};
  }
}
