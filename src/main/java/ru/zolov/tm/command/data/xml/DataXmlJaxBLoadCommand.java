package ru.zolov.tm.command.data.xml;

import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.entity.Domain;
import ru.zolov.tm.enumerated.PathEnum;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public class DataXmlJaxBLoadCommand extends AbstractCommand {

  private final String name = "dataload-xml-jaxb";
  private final String description = "Load data from xml file (jaxb)";

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public boolean secure() {
    return true;
  }

  @Override
  public void execute()
      throws
      JAXBException,
      EmptyStringException,
      EmptyRepositoryException {
    System.out.println(description);
    @NotNull
    final File file = new File(PathEnum.XML.getPath());
    @NotNull
    final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
    @NotNull
    final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
    @NotNull
    final Domain domain = (Domain)unmarshaller.unmarshal(file);
    serviceLocator.getDomainService().load(domain);
    System.out.println("Data loaded from " + file.getPath());
  }

  @Override
  public RoleType[] roles() {
    return new RoleType[]{RoleType.ADMIN, RoleType.USER};
  }
}
