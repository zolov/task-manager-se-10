package ru.zolov.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.entity.Domain;
import ru.zolov.tm.enumerated.PathEnum;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public class DataJsonJaksonSaveCommand extends AbstractCommand {

  private final String name = "datasave-json-jackson";
  private final String description = "Save data to json file (jackson)";

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public boolean secure() {
    return true;
  }

  @Override
  public void execute()
      throws
      EmptyStringException,
      EmptyRepositoryException,
      IOException {
    System.out.println(description);
    @NotNull
    final Domain domain = new Domain();
    serviceLocator.getDomainService().save(domain);
    @Nullable Path path = Paths.get(PathEnum.JSON.getPath());
    if (path == null) return;
    if (Files.notExists(path.getParent())) Files.createDirectory(path.getParent());
    @Nullable File file = path.toFile();
    if (file == null) return;
    @NotNull
    final ObjectMapper objectMapper = new ObjectMapper();
    @NotNull
    final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
    objectWriter.writeValue(file, domain);
    System.out.println("Data saved to " + file.getPath());
  }

  @Override
  public RoleType[] roles() {
    return new RoleType[]{RoleType.ADMIN, RoleType.USER};
  }
}
