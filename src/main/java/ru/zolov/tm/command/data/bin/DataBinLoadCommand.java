package ru.zolov.tm.command.data.bin;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import lombok.Cleanup;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.entity.Domain;
import ru.zolov.tm.enumerated.PathEnum;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public class DataBinLoadCommand extends AbstractCommand {

  private final String name = "dataload-bin";
  private final String description = "Load data from binary file";

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public boolean secure() {
    return true;
  }

  @Override
  public void execute()
      throws
      IOException,
      ClassNotFoundException,
      EmptyStringException,
      EmptyRepositoryException {
    System.out.println("Loading from file ");
    @Nullable
    final File file = new File(PathEnum.BIN.getPath());
    @Cleanup
    final FileInputStream fis = new FileInputStream(file);
    @Cleanup
    final ObjectInputStream ois = new ObjectInputStream(fis);
    @NotNull
    final Domain domain = (Domain)ois.readObject();
    serviceLocator.getDomainService().load(domain);
    System.out.println("DONE!");
  }

  @Override
  public RoleType[] roles() { return new RoleType[]{RoleType.ADMIN, RoleType.USER}; }
}
