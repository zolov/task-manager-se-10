package ru.zolov.tm.exception;

public class CommandCorruptException extends Exception {

  public CommandCorruptException() {
    super("Command is corrupt!");
  }
}
