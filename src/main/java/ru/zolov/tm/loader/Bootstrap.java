package ru.zolov.tm.loader;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.zolov.tm.api.IDomainService;
import ru.zolov.tm.api.IProjectRepository;
import ru.zolov.tm.api.IProjectService;
import ru.zolov.tm.api.ITaskRepository;
import ru.zolov.tm.api.ITaskService;
import ru.zolov.tm.api.ITerminalService;
import ru.zolov.tm.api.IUserRepository;
import ru.zolov.tm.api.IUserService;
import ru.zolov.tm.api.ServiceLocator;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.enumerated.StatusType;
import ru.zolov.tm.exception.CommandCorruptException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.UserExistException;
import ru.zolov.tm.exception.UserNotFoundException;
import ru.zolov.tm.repository.ProjectRepository;
import ru.zolov.tm.repository.TaskRepository;
import ru.zolov.tm.repository.UserRepository;
import ru.zolov.tm.service.DomainService;
import ru.zolov.tm.service.ProjectService;
import ru.zolov.tm.service.TaskService;
import ru.zolov.tm.service.TerminalService;
import ru.zolov.tm.service.UserService;

public final class Bootstrap implements ServiceLocator {

  private final IProjectRepository projectRepository = new ProjectRepository();
  private final ITaskRepository taskRepository = new TaskRepository();
  private final IProjectService projectService = new ProjectService(projectRepository, taskRepository);
  private final ITaskService taskService = new TaskService(taskRepository);
  private final ITerminalService terminalService = new TerminalService();
  private final IUserRepository userRepository = new UserRepository();
  private final IUserService userService = new UserService(userRepository);
  private final IDomainService domainService = new DomainService(this);
  private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
  private final Set<Class<? extends AbstractCommand>> classes = new Reflections("ru.zolov.tm").getSubTypesOf(AbstractCommand.class);

  @NotNull
  @Override
  public List<AbstractCommand> getCommandList() {
    List<AbstractCommand> commands = new ArrayList<>(this.commands.values());
    Collections.sort(commands);
    return commands;
  }

  @NotNull
  @Override
  public IUserService getUserService() {
    return userService;
  }

  @NotNull
  @Override
  public IDomainService getDomainService() {
    return domainService;
  }

  @NotNull
  @Override
  public IProjectService getProjectService() {
    return projectService;
  }

  @NotNull
  @Override
  public ITaskService getTaskService() {
    return taskService;
  }

  @NotNull
  @Override
  public ITerminalService getTerminalService() {
    return terminalService;
  }

  public void registry(@NotNull final Set<Class<? extends AbstractCommand>> classes)
      throws
      Exception {
    for (@NotNull final Class clazz : classes) {
      registry(clazz);
    }
  }

  public void registry(@NotNull final Class clazz)
      throws
      Exception {
    if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
    Object command = clazz.getDeclaredConstructor().newInstance();
    AbstractCommand abstractCommand = (AbstractCommand)command;
    registry(abstractCommand);
  }

  public void registry(AbstractCommand command)
      throws
      EmptyStringException {
    @NotNull
    final String cliCommand = command.getName();
    @NotNull
    final String cliDescription = command.getDescription();
    if (cliCommand == null || cliCommand.isEmpty()) throw new EmptyStringException();
    if (cliDescription == null || cliDescription.isEmpty()) throw new EmptyStringException();
    command.setServiceLocator(this);
    commands.put(cliCommand, command);
  }

  public void init()
      throws
      Exception {
    if (classes == null) throw new EmptyRepositoryException();
    BasicConfigurator.configure();
    registry(classes);
    initUsers();
    start();
  }

  public void start() {
    drawHeader();
    String command = "";
    while (true) {
      try {
        System.out.print("Enter command: \n");
        command = terminalService.nextLine();
        execute(command);
      } catch (Exception e) {
        System.err.println(e.getMessage());
        e.printStackTrace();
      }
    }
  }

  private void execute(String command)
      throws
      Exception {
    final AbstractCommand abstractCommand = commands.get(command);
    if (abstractCommand == null) throw new CommandCorruptException();
    final boolean rolesAllowed = getUserService().isRolesAllowed(abstractCommand.roles());
    final boolean secureCheck = abstractCommand.secure() || (!abstractCommand.secure() && userService.isAuth() && rolesAllowed);
    if (secureCheck) abstractCommand.execute();
    else System.out.println("Error! Access forbidden for this command");
  }

  private void initUsers()
      throws
      EmptyStringException,
      UserExistException,
      ParseException,
      EmptyRepositoryException,
      UserNotFoundException {
    userService.adminRegistration("admin", "admin");
    userService.userRegistration("user", "user");
    User admin = userService.findByLogin("admin");
    Project project1 = projectService.create(admin.getId(), "Google project", "Doing some good", "10.10.2010", "11.11.2011");
    Project project2 = projectService.create(admin.getId(), "Yandex project", "Doing some xopowo", "05.05.2010", "11.11.2011");
    Project project3 = projectService.create(admin.getId(), "Mail project", "Doing some ploho", "03.03.2010", "11.11.2011");
    project3.setStatus(StatusType.INPROGRESS);
    Project project4 = projectService.create(admin.getId(), "Amazon project", "Doing some dorogo", "01.10.2010", "11.11.2011");
    project4.setStatus(StatusType.INPROGRESS);
    Project project5 = projectService.create(admin.getId(), "Microsoft project", "Doing some neponyatno", "10.01.2010", "11.11.2011");
    project5.setStatus(StatusType.DONE);
    Task task = taskService.create(admin.getId(), project2.getId(), "Task in Yandex", "hard task", "13.10.2010", "15.10.2010");
    task.setStatus(StatusType.INPROGRESS);
    Task task1 = taskService.create(admin.getId(), project2.getId(), "Task in Yandex", "hard task", "10.10.2010", "15.10.2010");
    task1.setStatus(StatusType.INPROGRESS);
    Task task2 = taskService.create(admin.getId(), project1.getId(), "Task in google", "hard task", "12.10.2010", "15.10.2010");
    task2.setStatus(StatusType.INPROGRESS);
    Task task3 = taskService.create(admin.getId(), project1.getId(), "Task in google", "hard task", "15.10.2010", "21.10.2010");
    task3.setStatus(StatusType.INPROGRESS);
    taskService.create(admin.getId(), project1.getId(), "Task in google", "hard task", "16.10.2010", "22.10.2010");
    taskService.create(admin.getId(), project1.getId(), "Task in google", "hard task", "17.10.2010", "23.10.2010");
    taskService.create(admin.getId(), project1.getId(), "Task in google", "hard task", "18.10.2010", "24.10.2010");
    taskService.create(admin.getId(), project2.getId(), "Task in Yandex", "hard task", "14.10.2010", "15.10.2010");
    taskService.create(admin.getId(), project2.getId(), "Task in Yandex", "hard task", "12.10.2010", "15.10.2010");
    taskService.create(admin.getId(), project2.getId(), "Task in Yandex", "hard task", "16.10.2010", "15.10.2010");
  }

  private void drawHeader() {

    System.out.println("+----------------------------------------------------------------------------------------------------------+");
    System.out.println("|                Welcome to the task manager. Enter help to display a list of commands.                    |");
    System.out.println("+----------------------------------------------------------------------------------------------------------+");
  }
}