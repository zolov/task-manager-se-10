package ru.zolov.tm.service;

import java.text.ParseException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.ITaskRepository;
import ru.zolov.tm.api.ITaskService;
import ru.zolov.tm.entity.AbstractGoal;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public class TaskService extends AbstractService<Task> implements ITaskService {

  private ITaskRepository taskRepository;

  public TaskService(ITaskRepository taskRepository) {
    this.taskRepository = taskRepository;
  }

  @Override
  public ITaskRepository getTaskRepository() {
    return taskRepository;
  }

  @NotNull
  @Override
  public Task create(@Nullable final String userId,
                     @Nullable final String projectId,
                     @Nullable final String name,
                     @Nullable final String description,
                     @Nullable final String start,
                     @Nullable final String finish) throws EmptyStringException, ParseException {
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (name == null || name.isEmpty()) throw new EmptyStringException();
    if (projectId == null || projectId.isEmpty()) throw new EmptyStringException();
    if (start == null || start.isEmpty()) throw new EmptyStringException();
    if (finish == null || finish.isEmpty()) throw new EmptyStringException();
    Task task = new Task();
    task.setUserId(userId);
    task.setProjectId(projectId);
    task.setName(name);
    task.setDescription(description);
    task.setDateOfStart(dateFormat.parse(start));
    task.setDateOfFinish(dateFormat.parse(finish));
    taskRepository.persist(task);
    return task;
  }

  @NotNull
  @Override
  public List<Task> readAll(@Nullable String userId)
  throws EmptyRepositoryException, EmptyStringException {
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (taskRepository.findAll(userId) == null || taskRepository.findAll(userId).isEmpty())
      throw new EmptyRepositoryException();
    return taskRepository.findAll(userId);
  }

  @NotNull
  @Override
  public List<Task> readAll() throws EmptyRepositoryException {
    return taskRepository.findAll();
  }

  @Override
  public List<Task> readTaskByProjectId(@Nullable final String userId, @Nullable final String id)
  throws EmptyStringException, EmptyRepositoryException {
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    if (taskRepository.findTaskByProjId(userId, id) == null) throw new EmptyRepositoryException();
    return taskRepository.findTaskByProjId(userId, id);
  }

  @Override
  public Task readTaskById(@Nullable final String userId,
                           @Nullable final String projectId,
                           @Nullable final String id)
  throws EmptyStringException, EmptyRepositoryException {
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (projectId == null || projectId.isEmpty()) throw new EmptyStringException();
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    return taskRepository.findOne(userId, projectId, id);
  }

  @Override
  public boolean remove(@Nullable final String userId, @Nullable final String id)
  throws EmptyStringException {
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    return taskRepository.remove(userId, id);
  }

  @Override
  public void update(@Nullable final String userId,
                     @Nullable final String id,
                     @Nullable final String name,
                     @Nullable final String description,
                     @Nullable final String start,
                     @Nullable final String finish) throws EmptyStringException, ParseException {
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    if (name == null || name.isEmpty()) throw new EmptyStringException();
    final Date startDate = dateFormat.parse(start);
    final Date finishDate = dateFormat.parse(finish);
    taskRepository.update(userId, id, name, description, startDate, finishDate);
  }

  @NotNull
  public List<Task> sortBy(@Nullable final String userId,
                           @Nullable final String projectId,
                           @Nullable Comparator<AbstractGoal> comparator)
  throws EmptyStringException, EmptyRepositoryException {
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (projectId == null || projectId.isEmpty()) throw new EmptyStringException();
    final List<Task> list = taskRepository.findTaskByProjId(userId, projectId);
    Collections.sort(list, comparator);
    return list;
  }

  @Override
  public List<Task> findTask(@Nullable final String userId, @Nullable final String partOfTheName)
  throws EmptyRepositoryException, EmptyStringException {
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (partOfTheName == null) throw new EmptyStringException();
    if (partOfTheName.isEmpty()) return taskRepository.findAll(userId);
    final List<Task> list = taskRepository.findTask(userId, partOfTheName);
    if (list == null) throw new EmptyRepositoryException();
    return list;
  }

  public void load(@Nullable List<Task> list) throws EmptyRepositoryException {
    if (list != null) taskRepository.load(list);
  }
}
